New Zealand Orgs who use Ember
====================================

Sorted alphabetically by company name. To add yours, please see [the contributing guidelines](CONTRIBUTING.md).

- ANZ (Internal) https://anz.co.nz/
- Co-op bank https://www.co-operativebank.co.nz/
- Cruise guide http://www.cruiseguide.co.nz/marine-traffic/
- Haka Tours https://hakatours.com/
- Kordia (Internal) https://www.kordia.co.nz/
- Ministry of Social Development https://my.msd.co.nz/
- Mobile Studylink website https://my.studylink.govt.nz/mystudylink/logon_mobile.html
- My Work Sites https://myworksites.co.nz
- Real Estate NZ https://www.realestate.co.nz/
- ThreeNow https://www.threenow.co.nz/
- Travis CI https://travis-ci.org/
- TVNZ Ondemand https://www.tvnz.co.nz/shows
